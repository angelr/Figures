<?php

/**
* 
*/
class Circle extends figures implements iFigures
{
	
	function __construct($diameter)
    {
        parent::__construct(null, null, $diameter, 'Circle');
    }
    
    public function getArea(){
        $area = $this->getDiameter() / 2;
        $calculo = 3.14 * pow($area, 2);

        return $calculo;
    }
}