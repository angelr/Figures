<?php
//Interface
require ('Interface.php');
require ('Figures.php');
/**
* 
*/
//Figuras
require ('Square.php');
require ('Triangle.php');
require ('Circle.php');



class FiguresMaker 
{
	
	public function getFigure($base = null, $height = null, $diameter = null, $type = null){
		switch ($type) {
			case 'Square':
				return new Square($base, $height);
				break;
			case 'Circle':
				return new Circle($diameter);
				break;
			case 'Triangle':
				return new Triangle($base, $height);
				break;
			default:
				return 'Por favor seleccione una opción válida';
				break;
		}
	}
}