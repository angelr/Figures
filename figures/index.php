<?php


// Fabrica de figuras
require ('FiguresMaker.php');

class Controller 
{	
	function action(){
		$figure = new FiguresMaker();

		$square = $figure->getFigure(5, 7, null, 'Square');
		$triangle = $figure->getFigure(9, 8, null, 'Triangle');
		$circle = $figure->getFigure(null, null, 3, 'Circle');

		require ('view.php');
	}
}

$controller = new Controller();
$controller->action();


