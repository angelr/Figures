<?php


/**
* 
*/
class Square extends figures implements iFigures
{
	
	function __construct($base, $height)
    {
        parent::__construct($base, $height, null, 'Square');
    }

    public function getArea(){
    	return pow($this->getBase(), 2);
    }
}