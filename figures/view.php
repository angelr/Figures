<!DOCTYPE html>
<html>
  <head>
    <title>Figuras Geometricas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">

<style>
.fade {
opacity: 0;
-webkit-transition: opacity 2.25s linear;
  -moz-transition: opacity 2.25s linear;
   -ms-transition: opacity 2.25s linear;
	-o-transition: opacity 2.25s linear;
	   transition: opacity 2.25s linear;
}
.cuadrado {
     width: 100px; 
     height: 100px; 
     background: #428bca;
}
.circulo {
     width: 100px;
     height: 100px;
     -moz-border-radius: 50%;
     -webkit-border-radius: 50%;
     border-radius: 50%;
     background: #5cb85c;
}
.triangulo {
     width: 0; 
     height: 0; 
     border-left: 100px solid #f0ad4e;
     border-top: 50px solid transparent;
     border-bottom: 50px solid transparent; 
}
</style>
</head>
<body>
<div class="container">
<ul class="nav nav-tabs">
  <li class="active"><a href="#tab_a" data-toggle="tab">Cuadrado</a></li>
  <li><a href="#tab_b" data-toggle="tab">Triangulo</a></li>
  <li><a href="#tab_c" data-toggle="tab">Circulo</a></li>
</ul>
<div class="tab-content">
        <div class="tab-pane fade in active" id="tab_a">
            <h4>Cuadrado</h4>
            <p>*Base: <?php echo $square->getBase();?></p>
            <p>*Altura: <?php echo $square->getHeight();?></p>
            <p>*Area: <?php echo $square->getArea();?></p>
            <div class="cuadrado"></div>
        </div>
        <div class="tab-pane fade" id="tab_b">
            <h4>Triangulo</h4>
            <p>*Base: <?php echo $triangle->getBase();?></p>
            <p>*Altura: <?php echo $triangle->getHeight();?></p>
            <p>*Area: <?php echo $triangle->getArea();?></p>
            <div class="triangulo"></div>
        </div>
        <div class="tab-pane fade" id="tab_c">
            <h4>Circulo</h4>
            <p>*Diametro: <?php echo $circle->getDiameter();?></p>
            <p>*Area: <?php echo $circle->getArea();?></p>
            <div class="circulo"></div>
        </div>
      
</div><!-- tab content -->
</div><!-- end of container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>

