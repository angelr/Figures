<?php

/**
* 
*/
class Triangle extends figures implements iFigures
{
	function __construct($base, $height)
    {
        parent::__construct($base, $height, null, 'Triangle');
    }
    
    public function getArea(){
        return ($this->getBase() * $this->getHeight()) / 2;
    }
}